Media Entity Usage
--

This module and its submodules provides content editor information about usages of media entities.

Tested with `media_entity` at version `8.x-1.6`.

This is early development release.